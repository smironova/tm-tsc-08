package com.tsc.smironova.tm;

import com.tsc.smironova.tm.api.ICommandRepository;
import com.tsc.smironova.tm.constant.ArgumentConst;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.model.Command;
import com.tsc.smironova.tm.repository.CommandRepository;
import com.tsc.smironova.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    private final static ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        parseArgs(args);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public static void parseCommand(final String command) {
        if (command == null)
            return;
        switch (command) {
            case TerminalConstant.ABOUT -> showAbout();
            case TerminalConstant.VERSION -> showVersion();
            case TerminalConstant.HELP -> showHelp();
            case TerminalConstant.INFO -> showInfo();
            case TerminalConstant.COMMANDS -> showCommands();
            case TerminalConstant.ARGUMENTS -> showArguments();
            case TerminalConstant.EXIT -> exit();
            default -> showIncorrectArgument();
        }
    }

    public static void parseArg(final String arg) {
        if (arg == null)
            return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT -> showAbout();
            case ArgumentConst.ARG_VERSION -> showVersion();
            case ArgumentConst.ARG_HELP -> showHelp();
            case ArgumentConst.ARG_INFO ->  showInfo();
            default -> showIncorrectCommand();
        }
    }

    public static void parseArgs(final String[] args) {
        if (args == null || args.length == 0)
            return;
        final String arg = args[0];
        parseArg(arg);
        exit();
    }

    public static void showIncorrectArgument() {
        System.err.println("Error! Argument was not found!");
    }

    public static void showIncorrectCommand() {
        System.err.println("Error! Command was not found!");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Svetlana Mironova");
        System.out.println("E-MAIL: smironova@tsconsulting.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command.toString());
    }

    public static void showCommands() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands)
            System.out.println(command.getName());
    }

    public static void showArguments() {
        final Command[] commands = COMMAND_REPOSITORY.getTerminalCommands();
        for (final Command command : commands) {
            final String arg = command.getArgument();
            if (arg != null)
                System.out.println(arg);
        }
    }

    public static void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("[SYSTEM_INFO]");
        System.out.println("Available processors: " + processors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void exit() {
        System.exit(0);
    }

}
