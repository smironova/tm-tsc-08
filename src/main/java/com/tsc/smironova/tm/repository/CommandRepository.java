package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.ICommandRepository;
import com.tsc.smironova.tm.constant.ArgumentConst;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConstant.ABOUT, ArgumentConst.ARG_ABOUT, "Display developer info."
    );
    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConst.ARG_VERSION, "Display program version."
    );
    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConst.ARG_HELP, "Display list of terminal commands."
    );
    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConst.ARG_INFO, "Display system information."
    );
    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, null, "Show program commands."
    );
    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, null, "Show program arguments."
    );
    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null, "Close application."
    );
    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, VERSION, HELP, INFO, COMMANDS, ARGUMENTS, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
